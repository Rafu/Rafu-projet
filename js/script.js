$(function(){

    $(".navbar-nav a, footer a").on("click", function(event){

        event.preventDefault();
        var hash = this.hash;

        $('body,html').animate({scrollTop: $(hash).offset().top} , 900 , function(){window.location.hash = hash;})

    });

})

window.sr = ScrollReveal({ reset: true });

sr.reveal('.title h2, .title p, form, .timeline, .content p, .skills-content, .works');
